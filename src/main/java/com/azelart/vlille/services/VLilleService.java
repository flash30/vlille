package com.azelart.vlille.services;

import com.azelart.vlille.dto.StationDTO;
import com.azelart.vlille.dto.StationResponseDTO;
import com.azelart.vlille.exception.SynchronisationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Weather service.
 * @author Corentin Azelart
 */
@Service
@PropertySource("classpath:application.properties")
public class VLilleService {

    /**
     * Inject WS Endpoint for VLille.
     */
    @Value("${vlille.ws}")
    private String villeWs;

    /**
     * Find all stations status.
     * @return a list of stations.
     * @throws SynchronisationException if we can't synchronise the station list with remote API.
     */
    public StationResponseDTO findAll() throws SynchronisationException {
        final StationResponseDTO stationResponseDTO = new StationResponseDTO();

        // Check if we have stations in the map...
        final Map<Integer, StationDTO> stations = new HashMap<>();
        final List<StationDTO> loadedStations = this.performSynchronisation();

        // We add a station in map for each entry
        for(final StationDTO station : loadedStations) {
            stations.put(station.getIdx(), station);
        }

        stationResponseDTO.setCache(Boolean.FALSE);
        stationResponseDTO.setStations(loadedStations);

        return stationResponseDTO;
    }

    /**
     * Perform a synchronisation with CityBik API
     * @return a list of stations.
     * @throws SynchronisationException if we can't synchronise the station list with remote API.
     */
    public List<StationDTO> performSynchronisation() throws SynchronisationException {
        final RestTemplate restTemplate = new RestTemplate();
        final ParameterizedTypeReference<List<StationDTO>> parameterizedTypeReference = new ParameterizedTypeReference<List<StationDTO>>() {};
        final ResponseEntity<List<StationDTO>> response = restTemplate.exchange(villeWs, HttpMethod.GET, null, parameterizedTypeReference);
        return response.getBody();
    }
}
