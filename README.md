## FLASH30 : VLille with Docker and Rancher ##

### Sample
This is a web application to display [V'Lille](http://vlille.fr/stations/les-stations-vlille.aspx) bikes stations in a browser.
Hazelcast is used for cache, we use hazelcast-kubernetes-discovery for pods discovery (since Hazelcast 3.6).

### Slides
[Here](https://github.com/corentin59/hazelcast-with-docker-and-kubernetes/blob/master/slides/SLIDES.pdf)

### Build
[![Build Status](https://api.travis-ci.org/corentin59/hazelcast-with-docker-and-kubernetes.png)](https://travis-ci.org/corentin59/hazelcast-with-docker-and-kubernetes)

### Technical
* Tomcat 8 is the servlet container
* Java is the language
* Docker, Docker-compose and Rancher for deployment
* Maven for dependencies
* Sprint Boot for micro container
* AngularJS for front
* Hazelcast for cache and data replication

### Video
See here in HD : 

### Quick Boot
TODO

### Boot

#### 1. Build image :
The build is provided by Travis
https://travis-ci.org/corentin59/hazelcast-with-docker-and-kubernetes/

#### 2. Docker Hub :
You can pull the image from Docker hub
https://hub.docker.com/r/corentin59/hazelcast-with-docker-and-kubernetes/

## Thanks !
* Noctarius : https://github.com/noctarius/hazelcast-kubernetes-discovery
* Ray Tsang : https://www.youtube.com/watch?v=kT1vmK0r184

## License

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2015 <corentin@azelart.fr>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```

```
 This program is free software. It comes without any warranty, to
 the extent permitted by applicable law. You can redistribute it
 and/or modify it under the terms of the Do What The Fuck You Want
 To Public License, Version 2, as published by Corentin A. See
 http://www.wtfpl.net/ for more details.
```
